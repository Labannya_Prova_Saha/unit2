/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author NewPC
 */
public class SceneTest {
    
    public SceneTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod() {
        // TODO review the generated test code and remove the default call to fail.
                Scene scene = new Scene(5,3,5,3);
                scene.addTerrain("A","B", 0,1,0,2);
                Scene scene2 = new Scene(10,7,10,7);
                scene2.addTerrain("B","A",0,2,0,1);
        assertEquals("A,B", scene.getPoint(0,1,0,2));
        assertEquals("B,A", scene2.getPoint(0,2,0,1));
       // fail("The test case is a prototype.");
       
       //  Scene scene2 = new Scene(10, 5);
        
    }
    
}
