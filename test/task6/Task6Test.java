/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package task6; //unit2 task6

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author NewPC
 */
public class Task6Test {
    
    public Task6Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addPoint method, of class Task6.
     */

    /**
     * Test of isEmpty method, of class Task6.
     */
    @Test
    public void testIsEmpty() {
        Task6 task=new Task6(5,5);
        
        System.out.println("isEmpty");
        int row = 0;
        int column = 0;
        Task6 instance = null;
        boolean expResult = true;
        boolean result = task.isEmpty(3,0);
        assertEquals(true, result);
        // TODO review the generated test code and remove the default call to fail.
     //   fail("The test case is a prototype.");
    }

    /**
     * Test of countItems method, of class Task6.
     */
    @Test
    public void testCountItems() {
     Task6 task = new Task6(5,7);

     task.addPoint("A", 0, 0);
     task.addPoint("B", 1, 0);
     task.addPoint("C", 4, 6);
     task.addPoint("D", 0, 6);
     task.addPoint("E", 4, 0);
     task.addPoint("F", 2, 2);
     task.addPoint("G", 3, 3);
     task.addPoint("H", 2, 0);
     task.addPoint("I", 3, 6);
     task.addPoint("J", 4, 1);
     
     int i1 = task.countItems(0,0,1);
   System.out.println(i1);
    assertEquals(10, task.countItems(2,3,10));
    assertEquals(2, task.countItems(0,0,1));
    }
    
}
